#less3.3

<details><summary>Click to expand</summary>

1. Роль создана при поддержке ansible-galaxy
2. Переменные в vars
3. Шаблон конфиги в templates
4. Основная таска задействует шаблон и переменные
</details>


#update for less3.5 
<details><summary>Click to expand</summary>

1. Добавить Юзера на каждый из серверов - Name Ansible uid 1005 gid 1005
```
Добавил отдельную таску: roles/deploy_nginx/tasks/add_users.yml
И переменные в roles/deploy_nginx/vars/main.yml
```

2.Сменить Имя серверов на test1 test2
Добавил таску roles/deploy_nginx/tasks/change_hostname.yml и в переменную хостнеймы для серваков.
Сама таска:

```
---
  - name: "update hostname"
    hostname:
     name: "{{ hostnames[groups['srv-web'].index(inventory_hostname)] }}"
  - name: insert hostname in  /etc/hosts
    lineinfile:
      dest: /etc/hosts
      line: "127.0.0.1 {{ hostnames[groups['srv-web'].index(inventory_hostname)] }}"
      state: present 

```


3. Изменить файл hosts - добавить наши записи. Добавил отдельную таску kek.yml:

```
---
  - name: "hs"
    lineinfile:
      dest: /etc/hosts
      line:  "{{item.1}} {{ hostnames[item.0] }}"
    with_indexed_items:
      - "{{ groups['srv-web']}}"

```



4.  Модулем setup вывести на экран ip настройки ваших удвлённых серверов.
Использовал команду:
```
`shipovalovvs@work:~/juneway/ansible/less3.3$ ansible srv-web -i inventory.yml -m setup -a 'filter=ansible_all_ipv4_addresses'
[WARNING]: Invalid characters were found in group names but not replaced, use -vvvv to see details
34.125.171.227 | SUCCESS => {
    "ansible_facts": {
        "ansible_all_ipv4_addresses": [
            "10.182.0.7"
        ],
        "discovered_interpreter_python": "/usr/bin/python3"
    },
    "changed": false
}
34.125.116.23 | SUCCESS => {
    "ansible_facts": {
        "ansible_all_ipv4_addresses": [
            "10.182.0.6"
        ],
        "discovered_interpreter_python": "/usr/bin/python3"
    },
    "changed": false
}
```
</details>

#update for less3.6

0. Создал новые инстансы
1. Добавил os.yml в таски для установки пакетов в зависимости от ОС
2. Добавил time.yml в таски для изменения часового пояса в зависимости от ОС
3. Изменил шаблон, чтобы в нем добавляля айпи из фактов - server {{ ansible_default_ipv4.address }};
